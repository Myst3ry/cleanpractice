package com.myst3ry.cleanarchpractice.presenter;

import com.myst3ry.cleanarchpractice.data.mapper.ForecastDataModelMapper;
import com.myst3ry.cleanarchpractice.view.DetailView;
import com.myst3ry.domain.interactor.GetDailyForecastCase;
import com.myst3ry.domain.model.Forecast;

public final class DetailPresenter<T extends DetailView> {

    private T mView;
    private final GetDailyForecastCase mGetDailyForecastUseCase;

    public DetailPresenter(final GetDailyForecastCase dailyForecastUseCase) {
        this.mGetDailyForecastUseCase = dailyForecastUseCase;
    }

    public void attachView(T view) {
        this.mView = view;
    }

    public void getForecast(final int date) {
        mGetDailyForecastUseCase.execute(date, this::onForecastReceived);
    }

    private void onForecastReceived(final Forecast forecast) {
        mView.setForecastInfo(ForecastDataModelMapper.transform(forecast));
    }

    public void detachView() {
        this.mView = null;
    }

}

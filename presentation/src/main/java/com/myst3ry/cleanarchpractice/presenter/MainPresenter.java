package com.myst3ry.cleanarchpractice.presenter;

import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.widget.EditText;

import com.myst3ry.cleanarchpractice.data.mapper.ForecastDataModelMapper;
import com.myst3ry.cleanarchpractice.view.MainView;
import com.myst3ry.domain.interactor.GetForecastsCase;
import com.myst3ry.domain.model.Forecast;

import java.util.ArrayList;
import java.util.List;

public final class MainPresenter<T extends MainView> {

    private static final int MAX_DAYS_COUNT = 5;
    private static final int QUERY_DELAY = 1000;

    private T mView;
    private final GetForecastsCase mGetForecastsUseCase;

    private int mDaysCount;

    public MainPresenter(final GetForecastsCase forecastsUseCase) {
        this.mGetForecastsUseCase = forecastsUseCase;
    }

    public void attachView(T view) {
        this.mView = view;
    }

    public void getForecasts(final String city) {
        mView.showProgressBar();
        mGetForecastsUseCase.execute(city, this::onForecastsReceived);
    }

    public void getForecastsUpdates(final String city) {
        mGetForecastsUseCase.execute(city, this::onForecastsReceived);
    }

    private void onForecastsReceived(final List<Forecast> forecasts) {
        if (!forecasts.isEmpty()) {
            List<Forecast> filtered = new ArrayList<>(mDaysCount);
            if (mDaysCount != forecasts.size() && mDaysCount > 0) {
                for (int i = 0; i < mDaysCount; i++) {
                    filtered.add(forecasts.get(i));
                }
            } else {
                filtered = forecasts;
            }
            mView.setForecasts(ForecastDataModelMapper.transform(filtered));
        } else {
            mView.showError();
        }
        mView.hideProgressBar();
        mView.hideRefresher();
    }

    public void setCityChangeListener(final EditText citySelector, final EditText daysSelector) {
        citySelector.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                citySelector.setOnKeyListener((v, keyCode, event) -> {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        mDaysCount = Integer.valueOf(daysSelector.getText().toString());
                        new Handler(Looper.getMainLooper())
                                .postDelayed(() -> getForecastsUpdates(s.toString()), QUERY_DELAY);
                    }
                    return false;
                });
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void setDaysChangeListener(final EditText daysSelector, final EditText citySelector) {
        daysSelector.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                daysSelector.setOnKeyListener((v, keyCode, event) -> {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {

                        if (Integer.valueOf(s.toString()) > MAX_DAYS_COUNT) {
                            mDaysCount = MAX_DAYS_COUNT;
                            mView.setDaysCount(String.valueOf(MAX_DAYS_COUNT));
                        } else {
                            mDaysCount = Integer.valueOf(s.toString());
                        }

                        new Handler(Looper.getMainLooper())
                                .postDelayed(() -> getForecastsUpdates(citySelector.getText().toString()), QUERY_DELAY);
                    }
                    return false;
                });
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void detachView() {
        mView = null;
    }
}

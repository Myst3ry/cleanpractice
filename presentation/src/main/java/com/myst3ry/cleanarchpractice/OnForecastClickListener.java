package com.myst3ry.cleanarchpractice;

public interface OnForecastClickListener {

    void onForecastClick(final int id);
}

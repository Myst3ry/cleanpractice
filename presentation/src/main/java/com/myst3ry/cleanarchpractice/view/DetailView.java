package com.myst3ry.cleanarchpractice.view;

import com.myst3ry.cleanarchpractice.data.model.ForecastDataModel;

public interface DetailView {

    void setForecastInfo(final ForecastDataModel forecast);
}

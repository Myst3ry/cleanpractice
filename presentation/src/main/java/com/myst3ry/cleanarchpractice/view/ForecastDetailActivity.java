package com.myst3ry.cleanarchpractice.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.myst3ry.cleanarchpractice.BuildConfig;
import com.myst3ry.cleanarchpractice.R;
import com.myst3ry.cleanarchpractice.data.model.ForecastDataModel;
import com.myst3ry.cleanarchpractice.presenter.DetailPresenter;
import com.myst3ry.data.repository.ForecastRepositoryImpl;
import com.myst3ry.data.repository.datasource.ForecastsDataSourceFactory;
import com.myst3ry.domain.interactor.GetDailyForecastCase;
import com.myst3ry.domain.repository.ForecastRepository;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class ForecastDetailActivity extends AppCompatActivity implements DetailView {

    private static final String EXTRA_EPOCH_DATE = BuildConfig.APPLICATION_ID + "extra.EPOCH_DATE";

    @BindView(R.id.forecast_city)
    TextView mCityText;
    @BindView(R.id.forecast_day_of_week)
    TextView mWeekdayText;
    @BindView(R.id.forecast_date)
    TextView mDateText;
    @BindView(R.id.forecast_max_temp)
    TextView mMaxTempText;
    @BindView(R.id.forecast_day_phrase)
    TextView mDayPhraseText;
    @BindView(R.id.forecast_min_temp)
    TextView mMinTempText;
    @BindView(R.id.forecast_night_phrase)
    TextView mNightPhraseText;

    private DetailPresenter<ForecastDetailActivity> mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast_detail);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initPresenter();
    }

    @Override
    protected void onStart() {
        super.onStart();
        final int date = getIntent().getIntExtra(EXTRA_EPOCH_DATE, 0);
        mPresenter.getForecast(date);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setForecastInfo(final ForecastDataModel forecast) {
        mCityText.setText(forecast.getCity());
        mWeekdayText.setText(forecast.getDayOfWeek());
        mDateText.setText(forecast.getDate());
        mMaxTempText.setText(forecast.getTemperatureMax());
        mMinTempText.setText(forecast.getTemperatureMin());
        mDayPhraseText.setText(forecast.getDayPhrase());
        mNightPhraseText.setText(forecast.getNightPhrase());
    }

    private void initPresenter() {
        final ForecastsDataSourceFactory factory = new ForecastsDataSourceFactory(this);
        final ForecastRepository repository = ForecastRepositoryImpl.getInstance(factory);
        mPresenter = new DetailPresenter<>(new GetDailyForecastCase(repository));
        mPresenter.attachView(this);
    }

    public static Intent newExtraIntent(final Context context, final int date) {
        final Intent intent = newIntent(context);
        intent.putExtra(EXTRA_EPOCH_DATE, date);
        return intent;
    }

    public static Intent newIntent(final Context context) {
        return new Intent(context, ForecastDetailActivity.class);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isDestroyed()) {
            mPresenter.detachView();
        }
    }
}

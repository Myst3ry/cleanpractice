package com.myst3ry.cleanarchpractice.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myst3ry.cleanarchpractice.OnForecastClickListener;
import com.myst3ry.cleanarchpractice.R;
import com.myst3ry.cleanarchpractice.data.model.ForecastDataModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public final class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastItemHolder> {

    private final OnForecastClickListener mListener;
    private List<ForecastDataModel> mForecastList;

    public ForecastAdapter(final OnForecastClickListener listener) {
        this.mForecastList = new ArrayList<>();
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ForecastItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ForecastItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rec_forecast, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastItemHolder holder, int position) {
        final ForecastDataModel forecast = getDailyForecast(position);
        holder.mCity.setText(forecast.getCity());
        holder.mWeekday.setText(forecast.getDayOfWeek());
        holder.mDate.setText(forecast.getDate());
        holder.mAvgTemperature.setText(forecast.getTemperatureAvg());
    }

    @Override
    public int getItemCount() {
        return mForecastList.size();
    }

    public void setForecastList(final List<ForecastDataModel> forecasts) {
        this.mForecastList = forecasts;
        notifyDataSetChanged();
    }

    private ForecastDataModel getDailyForecast(final int position) {
        return mForecastList.get(position);
    }

    final class ForecastItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.city)
        TextView mCity;
        @BindView(R.id.weekday)
        TextView mWeekday;
        @BindView(R.id.date)
        TextView mDate;
        @BindView(R.id.avg_temp)
        TextView mAvgTemperature;

        @OnClick(R.id.weather_container)
        public void onClick() {
            final ForecastDataModel forecast = getDailyForecast(getLayoutPosition());
            mListener.onForecastClick(forecast.getId());
        }

        ForecastItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

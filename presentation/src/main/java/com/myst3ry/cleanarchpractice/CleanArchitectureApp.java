package com.myst3ry.cleanarchpractice;

import android.app.Application;
import android.content.Context;

import com.myst3ry.data.ResourcesManager;

public final class CleanArchitectureApp extends Application {

    private ResourcesManager mResourcesManager;

    @Override
    public void onCreate() {
        super.onCreate();
        initResourcesManager();
    }

    private void initResourcesManager() {
        mResourcesManager = new ResourcesManager(this);
    }

    public static ResourcesManager getResourcesManager(final Context context) {
        return ((CleanArchitectureApp) context.getApplicationContext()).mResourcesManager;
    }
}

package com.myst3ry.domain.callback;

import com.myst3ry.domain.model.Forecast;

public interface OnDataReceivedListener {

    void onDataReceived(final Forecast data);

}

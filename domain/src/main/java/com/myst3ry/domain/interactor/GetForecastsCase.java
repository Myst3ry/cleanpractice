package com.myst3ry.domain.interactor;

import com.myst3ry.domain.callback.OnDataListReceivedListener;
import com.myst3ry.domain.repository.ForecastRepository;

public final class GetForecastsCase {

    private final ForecastRepository mForecastRepository;

    public GetForecastsCase(final ForecastRepository repository) {
        this.mForecastRepository = repository;
    }

    public void execute(final String city, final OnDataListReceivedListener callback) {
        mForecastRepository.requestAllForecasts(city, callback);
    }
}

package com.myst3ry.domain.interactor;

import com.myst3ry.domain.callback.OnDataReceivedListener;
import com.myst3ry.domain.repository.ForecastRepository;

public final class GetDailyForecastCase {

    private final ForecastRepository mForecastRepository;

    public GetDailyForecastCase(final ForecastRepository repository) {
        this.mForecastRepository = repository;
    }

    public void execute(final int epochDate, final OnDataReceivedListener callback) {
        mForecastRepository.requestDailyForecast(epochDate, callback);
    }
}

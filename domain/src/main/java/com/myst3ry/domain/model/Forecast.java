package com.myst3ry.domain.model;

/**
 * Model class that represents a Forecast in the domain layer.
 */
public final class Forecast {

    private int mEpochDate;
    private String mDate;
    private int mTemperatureMax;
    private int mTemperatureMin;
    private String mUnit;
    private String mDayPhrase;
    private String mNightPhrase;
    private String mCity;

    public Forecast(int epochDate, String date, int temperatureMax, int temperatureMin,
                    String tempUnit, String dayPhrase, String nightPhrase, String city) {
        this.mEpochDate = epochDate;
        this.mDate = date;
        this.mTemperatureMax = temperatureMax;
        this.mTemperatureMin = temperatureMin;
        this.mUnit = tempUnit;
        this.mDayPhrase = dayPhrase;
        this.mNightPhrase = nightPhrase;
        this.mCity = city;
    }

    public int getEpochDate() {
        return mEpochDate;
    }

    public void setEpochDate(final int epochDate) {
        this.mEpochDate = epochDate;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(final String date) {
        this.mDate = date;
    }

    public int getTemperatureMax() {
        return mTemperatureMax;
    }

    public void setTemperatureMax(final int temperatureMax) {
        this.mTemperatureMax = temperatureMax;
    }

    public int getTemperatureMin() {
        return mTemperatureMin;
    }

    public void setTemperatureMin(final int temperatureMin) {
        this.mTemperatureMin = temperatureMin;
    }

    public String getUnit() {
        return mUnit;
    }

    public void setUnit(final String tempUnit) {
        this.mUnit = tempUnit;
    }

    public String getDayPhrase() {
        return mDayPhrase;
    }

    public void setDayPhrase(final String dayPhrase) {
        this.mDayPhrase = dayPhrase;
    }

    public String getNightPhrase() {
        return mNightPhrase;
    }

    public void setNightPhrase(final String nightPhrase) {
        this.mNightPhrase = nightPhrase;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(final String city) {
        this.mCity = city;
    }

    @Override
    public String toString() {
        return "Forecast {" +
                "EpochDate=" + mEpochDate +
                ", Date='" + mDate + '\'' +
                ", TemperatureMax=" + mTemperatureMax +
                ", TemperatureMin=" + mTemperatureMin +
                ", TempUnit=" + mUnit + '\'' +
                ", DayPhrase='" + mDayPhrase + '\'' +
                ", NightPhrase='" + mNightPhrase + '\'' +
                ", City='" + mCity + '\'' +
                '}';
    }
}

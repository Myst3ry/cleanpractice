package com.myst3ry.domain.repository;

import com.myst3ry.domain.callback.OnDataListReceivedListener;
import com.myst3ry.domain.callback.OnDataReceivedListener;

public interface ForecastRepository {

    void requestAllForecasts(final String city, final OnDataListReceivedListener dataListCallback);

    void requestDailyForecast(final int epochDate, final OnDataReceivedListener dataCallback);
}

package com.myst3ry.data.entity.mapper;

import com.myst3ry.data.entity.ForecastEntity;
import com.myst3ry.data.remote.model.ForecastModel;

import java.util.ArrayList;
import java.util.List;

public final class ForecastEntityMapper {

    public static ForecastEntity transform(final ForecastModel model, final String city) {
        return new ForecastEntity(
                model.getEpochDate(),
                model.getDate(),
                Math.round(model.getTemperature().getMaximum().getValue()),
                Math.round(model.getTemperature().getMinimum().getValue()),
                model.getTemperature().getMinimum().getUnit(),
                model.getDay().getIconPhrase(),
                model.getNight().getIconPhrase(),
                city);
    }

    public static List<ForecastEntity> transform(final List<ForecastModel> models, final String city) {
        final List<ForecastEntity> entities = new ArrayList<>();
        for (final ForecastModel model : models) {
            entities.add(transform(model, city));
        }

        return entities;
    }

    private ForecastEntityMapper() {
    }
}

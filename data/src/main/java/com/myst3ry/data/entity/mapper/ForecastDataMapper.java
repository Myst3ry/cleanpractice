package com.myst3ry.data.entity.mapper;

import com.myst3ry.data.entity.ForecastEntity;
import com.myst3ry.domain.model.Forecast;

import java.util.ArrayList;
import java.util.List;

public final class ForecastDataMapper {

    public static Forecast transform(final ForecastEntity entity) {
        return new Forecast(
                entity.getEpochDate(),
                entity.getDate(),
                entity.getTemperatureMax(),
                entity.getTemperatureMin(),
                entity.getUnit(),
                entity.getDayPhrase(),
                entity.getNightPhrase(),
                entity.getCity());
    }

    public static List<Forecast> transform(final List<ForecastEntity> entities) {
        final List<Forecast> data = new ArrayList<>();
        for (final ForecastEntity entity : entities) {
            data.add(transform(entity));
        }
        return data;
    }

    private ForecastDataMapper() {
    }

}

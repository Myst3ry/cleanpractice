package com.myst3ry.data.repository.datasource;

import android.content.Context;
import android.support.annotation.NonNull;

import com.myst3ry.data.NetworkManager;
import com.myst3ry.data.OnForecastsReceivedListener;
import com.myst3ry.data.ResourcesManager;
import com.myst3ry.data.executor.DatabaseExecutor;
import com.myst3ry.data.local.SQLiteDatabaseHelper;
import com.myst3ry.data.local.SQLiteDatabaseManager;
import com.myst3ry.data.remote.ApiMapper;

public final class ForecastsDataSourceFactory {

    private final Context mContext;

    public ForecastsDataSourceFactory(@NonNull final Context context) {
        this.mContext = context;
    }

    public ForecastsDataSource create(final OnForecastsReceivedListener callback) {
        ForecastsDataSource dataSource;
        if (NetworkManager.isNetworkAvailable(mContext)) {
            dataSource = createRemoteDataSource(callback);
        } else {
            dataSource = createLocalDataSource(callback);
        }
        return dataSource;
    }

    public ForecastsDataSource createLocalDataSource(final OnForecastsReceivedListener callback) {
        final SQLiteDatabaseHelper databaseHelper = SQLiteDatabaseHelper.getInstance(mContext);
        final SQLiteDatabaseManager databaseManager = SQLiteDatabaseManager.getInstance(databaseHelper, callback);
        final DatabaseExecutor databaseExecutor = new DatabaseExecutor();
        return new ForecastsLocalDataSource(databaseManager, databaseExecutor);
    }

    public ForecastsDataSource createRemoteDataSource(final OnForecastsReceivedListener callback) {
        final ResourcesManager resourcesManager = new ResourcesManager(mContext);
        final ApiMapper apiMapper = ApiMapper.getInstance(resourcesManager, callback);
        return new ForecastsRemoteDataSource(apiMapper);
    }
}

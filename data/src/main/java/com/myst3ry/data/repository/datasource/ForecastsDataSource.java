package com.myst3ry.data.repository.datasource;

public interface ForecastsDataSource {

    void requestAllForecasts(final String city);

    void requestDailyForecast(final int epochDate);
}

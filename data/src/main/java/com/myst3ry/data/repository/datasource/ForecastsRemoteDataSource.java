package com.myst3ry.data.repository.datasource;

import com.myst3ry.data.remote.ApiMapper;

public final class ForecastsRemoteDataSource implements ForecastsDataSource {

    private final ApiMapper mApiMapper;

    public ForecastsRemoteDataSource(final ApiMapper apiMapper) {
        this.mApiMapper = apiMapper;
    }

    @Override
    public void requestAllForecasts(final String city) {
        mApiMapper.getForecastsRemote(city);
    }

    @Override
    public void requestDailyForecast(final int epochDate) {
        throw new UnsupportedOperationException("Operation is not supported");
    }
}

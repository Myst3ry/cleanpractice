package com.myst3ry.data.repository.datasource;

import com.myst3ry.data.entity.ForecastEntity;
import com.myst3ry.data.executor.DatabaseExecutor;
import com.myst3ry.data.local.SQLiteDatabaseManager;

import java.util.List;

public final class ForecastsLocalDataSource implements ForecastsDataSource {

    private final SQLiteDatabaseManager mDatabaseManager;
    private final DatabaseExecutor mExecutor;

    public ForecastsLocalDataSource(final SQLiteDatabaseManager databaseManager, final DatabaseExecutor executor) {
        this.mDatabaseManager = databaseManager;
        this.mExecutor = executor;
    }

    @Override
    public void requestAllForecasts(final String city) {
        mExecutor.execute(() -> mDatabaseManager.getAllForecasts(city));
    }

    @Override
    public void requestDailyForecast(final int epochDate) {
        mExecutor.execute(() -> mDatabaseManager.getDailyForecast(epochDate));
    }

    public void saveAllForecasts(final List<ForecastEntity> entities) {
        mExecutor.execute(() -> mDatabaseManager.saveForecasts(entities));
    }

}

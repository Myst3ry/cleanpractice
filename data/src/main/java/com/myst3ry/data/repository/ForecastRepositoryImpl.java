package com.myst3ry.data.repository;

import com.myst3ry.data.OnForecastsReceivedListener;
import com.myst3ry.data.entity.ForecastEntity;
import com.myst3ry.data.entity.mapper.ForecastDataMapper;
import com.myst3ry.data.repository.datasource.ForecastsDataSourceFactory;
import com.myst3ry.data.repository.datasource.ForecastsLocalDataSource;
import com.myst3ry.domain.callback.OnDataListReceivedListener;
import com.myst3ry.domain.callback.OnDataReceivedListener;
import com.myst3ry.domain.repository.ForecastRepository;

import java.util.List;

public final class ForecastRepositoryImpl implements ForecastRepository, OnForecastsReceivedListener {

    private final ForecastsDataSourceFactory mDataSourceFactory;
    private OnDataReceivedListener mDataCallback;
    private OnDataListReceivedListener mDataListCallback;

    private static volatile ForecastRepositoryImpl INSTANCE;

    public static ForecastRepositoryImpl getInstance(final ForecastsDataSourceFactory factory) {
        ForecastRepositoryImpl instance = INSTANCE;
        if (instance == null) {
            synchronized (ForecastRepositoryImpl.class) {
                instance = INSTANCE;
                if (instance == null) {
                    instance = INSTANCE = new ForecastRepositoryImpl(factory);
                }
            }
        }
        return instance;
    }

    private ForecastRepositoryImpl(final ForecastsDataSourceFactory factory) {
        this.mDataSourceFactory = factory;
    }

    @Override
    public void requestAllForecasts(final String city, OnDataListReceivedListener dataListCallback) {
        this.mDataListCallback = dataListCallback;
        mDataSourceFactory.create(this).requestAllForecasts(city);
    }

    //always from local
    @Override
    public void requestDailyForecast(final int epochDate, OnDataReceivedListener dataCallback) {
        this.mDataCallback = dataCallback;
        mDataSourceFactory.createLocalDataSource(this).requestDailyForecast(epochDate);
    }

    @Override
    public void onForecastsReceived(final List<ForecastEntity> entities) {
        if (entities != null) {
            ((ForecastsLocalDataSource) mDataSourceFactory.createLocalDataSource(this)).saveAllForecasts(entities);
            mDataListCallback.onDataListReceived(ForecastDataMapper.transform(entities));
        }
    }

    @Override
    public void onForecastReceived(final ForecastEntity entity) {
        if (mDataCallback != null) {
            mDataCallback.onDataReceived(ForecastDataMapper.transform(entity));
        }
    }
}

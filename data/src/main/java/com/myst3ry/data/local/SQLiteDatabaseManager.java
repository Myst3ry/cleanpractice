package com.myst3ry.data.local;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.WorkerThread;
import android.util.Log;

import com.myst3ry.data.OnForecastsReceivedListener;
import com.myst3ry.data.entity.ForecastEntity;

import java.util.ArrayList;
import java.util.List;

public final class SQLiteDatabaseManager {

    private static final String TAG = "SQLiteDatabaseManager";
    private static final String DATE_WHERE_CLAUSE = SQLiteDatabaseContract.COLUMN_EPOCH_DATE + " = ?";
    private static final String CITY_WHERE_CLAUSE = SQLiteDatabaseContract.COLUMN_CITY + " = ?";

    private final SQLiteDatabaseHelper mHelper;
    private final OnForecastsReceivedListener mCallback;

    private static volatile SQLiteDatabaseManager INSTANCE;

    public static SQLiteDatabaseManager getInstance(final SQLiteDatabaseHelper helper, final OnForecastsReceivedListener callback) {
        SQLiteDatabaseManager instance = INSTANCE;
        if (instance == null) {
            synchronized (SQLiteDatabaseManager.class) {
                instance = INSTANCE;
                if (instance == null) {
                    instance = INSTANCE = new SQLiteDatabaseManager(helper, callback);
                }
            }
        }
        return instance;
    }

    private SQLiteDatabaseManager(final SQLiteDatabaseHelper helper, final OnForecastsReceivedListener callback) {
        this.mHelper = helper;
        this.mCallback = callback;
    }

    @WorkerThread
    public void getAllForecasts(final String city) {
        SQLiteDatabase mDatabase = null;
        try {
            mDatabase = mHelper.getReadableDatabase();
            mDatabase.beginTransaction();

            final Cursor cursor = mDatabase.query(SQLiteDatabaseContract.FORECASTS_TABLE, null, CITY_WHERE_CLAUSE,
                    new String[]{city}, null, null, SQLiteDatabaseContract.COLUMN_EPOCH_DATE + " ASC");
            final List<ForecastEntity> entities = parseForecastList(cursor);
            cursor.close();

            mDatabase.setTransactionSuccessful();
            new Handler(Looper.getMainLooper()).post(() -> mCallback.onForecastsReceived(entities));
        } catch (SQLiteException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
            }
        }
    }

    @WorkerThread
    public void getDailyForecast(final int epochDate) {
        SQLiteDatabase mDatabase = null;
        try {
            mDatabase = mHelper.getReadableDatabase();
            mDatabase.beginTransaction();

            final String date = String.valueOf(epochDate);
            final Cursor cursor = mDatabase.query(SQLiteDatabaseContract.FORECASTS_TABLE, null, DATE_WHERE_CLAUSE,
                    new String[]{date}, null, null, null);
            final ForecastEntity entity = parseForecastCursor(cursor);
            cursor.close();

            mDatabase.setTransactionSuccessful();
            new Handler(Looper.getMainLooper()).post(() -> mCallback.onForecastReceived(entity));
        } catch (SQLiteException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
            }
        }
    }

    @WorkerThread
    public void saveForecasts(final List<ForecastEntity> forecasts) {
        SQLiteDatabase mDatabase = null;
        try {
            mDatabase = mHelper.getWritableDatabase();
            mDatabase.beginTransaction();

            mDatabase.delete(SQLiteDatabaseContract.FORECASTS_TABLE, null, null);
            for (final ForecastEntity forecast : forecasts) {
                final ContentValues values = getForecastContentValues(forecast);
                mDatabase.insert(SQLiteDatabaseContract.FORECASTS_TABLE, null, values);
            }

            mDatabase.setTransactionSuccessful();

        } catch (SQLiteException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
            }
        }
    }

    private ContentValues getForecastContentValues(final ForecastEntity entity) {
        final int forecastEpochDate = entity.getEpochDate();
        final String forecastDate = entity.getDate();
        final int forecastTempMin = entity.getTemperatureMin();
        final int forecastTempMax = entity.getTemperatureMax();
        final String forecastTempUnit = entity.getUnit();
        final String forecastDayPhrase = entity.getDayPhrase();
        final String forecastNightPhrase = entity.getNightPhrase();
        final String forecastCity = entity.getCity();

        final ContentValues forecastContentValues = new ContentValues();

        forecastContentValues.put(SQLiteDatabaseContract.COLUMN_EPOCH_DATE, forecastEpochDate);
        forecastContentValues.put(SQLiteDatabaseContract.COLUMN_DATE, forecastDate);
        forecastContentValues.put(SQLiteDatabaseContract.COLUMN_TEMP_MIN, forecastTempMin);
        forecastContentValues.put(SQLiteDatabaseContract.COLUMN_TEMP_MAX, forecastTempMax);
        forecastContentValues.put(SQLiteDatabaseContract.COLUMN_TEMP_UNIT, forecastTempUnit);
        forecastContentValues.put(SQLiteDatabaseContract.COLUMN_DAY_PHRASE, forecastDayPhrase);
        forecastContentValues.put(SQLiteDatabaseContract.COLUMN_NIGHT_PHRASE, forecastNightPhrase);
        forecastContentValues.put(SQLiteDatabaseContract.COLUMN_CITY, forecastCity);

        return forecastContentValues;
    }

    private ForecastEntity parseForecastCursor(final Cursor cursor) {
        ForecastEntity entity = null;
        if (cursor.moveToFirst()) {
            entity = getForecastFromCursor(cursor);
        }
        return entity;
    }

    private List<ForecastEntity> parseForecastList(final Cursor cursor) {
        final List<ForecastEntity> entities = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                entities.add(getForecastFromCursor(cursor));
                cursor.moveToNext();
            }
        }
        return entities;
    }

    private ForecastEntity getForecastFromCursor(final Cursor cursor) {
        final int epochDate = cursor.getInt(cursor.getColumnIndex(SQLiteDatabaseContract.COLUMN_EPOCH_DATE));
        final int minTemp = cursor.getInt(cursor.getColumnIndex(SQLiteDatabaseContract.COLUMN_TEMP_MIN));
        final int maxTemp = cursor.getInt(cursor.getColumnIndex(SQLiteDatabaseContract.COLUMN_TEMP_MAX));
        final String unit = cursor.getString(cursor.getColumnIndex(SQLiteDatabaseContract.COLUMN_TEMP_UNIT));
        final String city = cursor.getString(cursor.getColumnIndex(SQLiteDatabaseContract.COLUMN_CITY));
        final String date = cursor.getString(cursor.getColumnIndex(SQLiteDatabaseContract.COLUMN_DATE));
        final String dayPhrase = cursor.getString(cursor.getColumnIndex(SQLiteDatabaseContract.COLUMN_DAY_PHRASE));
        final String nightPhrase = cursor.getString(cursor.getColumnIndex(SQLiteDatabaseContract.COLUMN_NIGHT_PHRASE));

        return new ForecastEntity(epochDate, date, maxTemp, minTemp, unit, dayPhrase, nightPhrase, city);
    }
}

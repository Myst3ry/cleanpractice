package com.myst3ry.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class SQLiteDatabaseHelper extends SQLiteOpenHelper {

    private final static int DB_VERSION = 1;
    private final static String DB_NAME = "weather_forecasts_db";

    private static volatile SQLiteDatabaseHelper INSTANCE;

    public static SQLiteDatabaseHelper getInstance(final Context context) {
        SQLiteDatabaseHelper instance = INSTANCE;
        if (instance == null) {
            synchronized (SQLiteDatabaseHelper.class) {
                instance = INSTANCE;
                if (instance == null) {
                    instance = INSTANCE = new SQLiteDatabaseHelper(context);
                }
            }
        }
        return instance;
    }

    private SQLiteDatabaseHelper(final Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTables(db);
        createTables(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    private void createTables(final SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + SQLiteDatabaseContract.FORECASTS_TABLE + " (" + SQLiteDatabaseContract.COLUMN_EPOCH_DATE
                + " INTEGER PRIMARY KEY, " + SQLiteDatabaseContract.COLUMN_DATE + " TEXT, " + SQLiteDatabaseContract.COLUMN_TEMP_MIN
                + " INTEGER, " + SQLiteDatabaseContract.COLUMN_TEMP_MAX + " INTEGER, " + SQLiteDatabaseContract.COLUMN_TEMP_UNIT
                + " TEXT, " + SQLiteDatabaseContract.COLUMN_DAY_PHRASE + " TEXT, " + SQLiteDatabaseContract.COLUMN_NIGHT_PHRASE
                + " TEXT, " + SQLiteDatabaseContract.COLUMN_CITY + " TEXT)");
    }

    private void dropTables(final SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + SQLiteDatabaseContract.FORECASTS_TABLE);
    }
}

package com.myst3ry.data.local;

public final class SQLiteDatabaseContract {

    public static final String FORECASTS_TABLE = "forecasts";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_EPOCH_DATE = "epoch_date";
    public static final String COLUMN_TEMP_MIN = "min_temperature";
    public static final String COLUMN_TEMP_MAX = "max_temperature";
    public static final String COLUMN_TEMP_UNIT = "temperature_unit";
    public static final String COLUMN_DAY_PHRASE = "day_phrase";
    public static final String COLUMN_NIGHT_PHRASE = "night_phrase";
    public static final String COLUMN_CITY = "city";

    private SQLiteDatabaseContract() {
    }
}

package com.myst3ry.data;

import android.content.Context;
import android.content.res.Resources;

public final class ResourcesManager {

    private final Resources mResources;

    public ResourcesManager(final Context context) {
        this.mResources = context.getResources();
    }

    public String getCountry() {
        return mResources.getString(R.string.country);
    }

    public String getCity() {
        return mResources.getString(R.string.city);
    }

    public String getLanguage() {
        return mResources.getString(R.string.language);
    }

    public boolean isMetricEnabled() {
        return mResources.getBoolean(R.bool.metrics);
    }
}

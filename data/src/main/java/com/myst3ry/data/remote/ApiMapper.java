package com.myst3ry.data.remote;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.myst3ry.data.OnForecastsReceivedListener;
import com.myst3ry.data.ResourcesManager;
import com.myst3ry.data.entity.ForecastEntity;
import com.myst3ry.data.entity.mapper.ForecastEntityMapper;
import com.myst3ry.data.remote.model.City;
import com.myst3ry.data.remote.model.ForecastModel;
import com.myst3ry.data.remote.model.ForecastResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public final class ApiMapper {

    private static final int FORECASTS_API_VERSION = 1;
    private static final int LOCATIONS_API_VERSION = 1;
    private static final String API_KEY = "vxGdPOeH9Gp8ydsfqiBtK0cLhx5x3EgG";
    private static final String TAG = "ApiMapper";

    private final ResourcesManager mResourcesManager;
    private final RetrofitHelper mHelper;
    private final OnForecastsReceivedListener mCallback;

    private static volatile ApiMapper INSTANCE;

    public static ApiMapper getInstance(final ResourcesManager resourcesManager, final OnForecastsReceivedListener callback) {
        ApiMapper instance = INSTANCE;
        if (instance == null) {
            synchronized (ApiMapper.class) {
                instance = INSTANCE;
                if (instance == null) {
                    instance = INSTANCE = new ApiMapper(resourcesManager, callback);
                }
            }
        }
        return instance;
    }

    private ApiMapper(final ResourcesManager resourcesManager, final OnForecastsReceivedListener callback) {
        this.mHelper = RetrofitHelper.getInstance();
        this.mResourcesManager = resourcesManager;
        this.mCallback = callback;
    }

    public void getForecastsRemote(@Nullable final String cityQuery) {
        this.getCitiesByQuery(cityQuery);
    }

    private void getCitiesByQuery(@Nullable final String cityQuery) {
        mHelper.getService().getCitiesByQuery(LOCATIONS_API_VERSION, API_KEY,
                cityQuery != null ? cityQuery : mResourcesManager.getCity(), mResourcesManager.getLanguage())
                .enqueue(new Callback<List<City>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<City>> call, @NonNull Response<List<City>> response) {
                        if (response.isSuccessful()) {
                            final List<City> cities = response.body();
                            if (cities != null && !cities.isEmpty()) {
                                final City city = cities.get(0);
                                getForecastsByLocation(city.getKey(), city.getLocalizedName());
                            } else {
                                mCallback.onForecastsReceived(new ArrayList<>());
                            }
                        } else {
                            Log.e(TAG, response.code() + " " + response.message());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<City>> call, @NonNull Throwable t) {
                        t.printStackTrace();
                        Log.e(TAG, t.getLocalizedMessage());
                    }
                });
    }

    private void getForecastsByLocation(@NonNull final String locationKey, final String cityName) {
        mHelper.getService().getWeatherForecast(FORECASTS_API_VERSION, locationKey,
                API_KEY, mResourcesManager.getLanguage(), mResourcesManager.isMetricEnabled())
                .enqueue(new Callback<ForecastResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<ForecastResponse> call, @NonNull Response<ForecastResponse> response) {
                        if (response.isSuccessful()) {
                            final List<ForecastModel> models = Objects.requireNonNull(response.body()).getForecasts();
                            final List<ForecastEntity> entities = ForecastEntityMapper.transform(models, cityName);
                            mCallback.onForecastsReceived(entities);
                        } else {
                            Log.e(TAG, response.code() + " " + response.message());
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<ForecastResponse> call, @NonNull Throwable t) {
                        t.printStackTrace();
                        Log.e(TAG, t.getLocalizedMessage());
                    }
                });
    }
}
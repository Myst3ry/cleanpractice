package com.myst3ry.data.remote.model;

import com.google.gson.annotations.SerializedName;

public final class Day {

    @SerializedName("IconPhrase")
    private String iconPhrase;

    @SerializedName("Icon")
    private int icon;

    public String getIconPhrase() {
        return iconPhrase;
    }

    public int getIcon() {
        return icon;
    }
}
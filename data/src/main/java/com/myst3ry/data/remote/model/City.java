package com.myst3ry.data.remote.model;

import com.google.gson.annotations.SerializedName;

public final class City {

    @SerializedName("Key")
    private String key;

    @SerializedName("LocalizedName")
    private String localizedName;

    @SerializedName("EnglishName")
    private String englishName;

    @SerializedName("Country")
    private Country country;


    public String getLocalizedName() {
        return localizedName;
    }

    public Country getCountry() {
        return country;
    }

    public String getKey() {
        return key;
    }

    public String getEnglishName() {
        return englishName;
    }
}
package com.myst3ry.data.remote.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class ForecastResponse {

    @SerializedName("DailyForecasts")
    private List<ForecastModel> forecasts;

    public List<ForecastModel> getForecasts() {
        return forecasts;
    }
}
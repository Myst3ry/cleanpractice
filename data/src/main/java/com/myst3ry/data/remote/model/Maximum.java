package com.myst3ry.data.remote.model;

import com.google.gson.annotations.SerializedName;

public final class Maximum {

    @SerializedName("Value")
    private float value;

    @SerializedName("Unit")
    private String unit;

    public float getValue() {
        return value;
    }

    public String getUnit() {
        return unit;
    }
}
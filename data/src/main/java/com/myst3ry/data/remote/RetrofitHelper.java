package com.myst3ry.data.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class RetrofitHelper {

    private static volatile RetrofitHelper INSTANCE;

    public static RetrofitHelper getInstance() {
        RetrofitHelper instance = INSTANCE;
        if (instance == null) {
            synchronized (RetrofitHelper.class) {
                instance = INSTANCE;
            }
            if (instance == null) {
                instance = INSTANCE = new RetrofitHelper();
            }
        }
        return instance;
    }

    private RetrofitHelper() {
    }

    public ForecastsApi getService() {
        final Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ForecastsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(ForecastsApi.class);
    }
}